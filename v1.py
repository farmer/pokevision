import math

class Vec2:
    x = 0
    y = 0

    def Add(self, v):
        self.x += v.x
        self.y += v.y

    def __init__(self, xInit, yInit):
        self.x = xInit
        self.y = yInit

class Vec3:
    x = 0
    y = 0
    z = 0

    def Add(self, v):
        self.x += v.x
        self.y += v.y
        self.z += v.z

    def Divide(self, n):
        self.x /= n
        self.y /= n
        self.z /= n

    def __init__(self, xInit, yInit, zInit):
        self.x = xInit;
        self.y = yInit
        self.z = zInit

def Distance(v0, v1):
    vDiff = Vec3(v1.x - v0.x, v1.y - v0.y, v1.z - v0.z)
    return math.sqrt(vDiff.x*vDiff.x + vDiff.y*vDiff.y + vDiff.z*vDiff.z)

class Quadtree:
    boxMin = Vec2(0, 0);
    boxMax = Vec2(0, 0);
    average = Vec3(0, 0, 0);
    tl = 0
    tr = 0
    bl = 0
    br = 0

    def CalculateAverageColor(self, image, boxMin, boxMax):
        color = Vec3(0, 0, 0)
        pixelCount = 0
        for x in range(boxMin.x, boxMax.x):
            for y in range(boxMin.y, boxMax.y):
                Vec3.Add(pixel.r, pixel.g, pixel.b)
                pixelCount += 1
        return average.Divide(pixelCount)

    #self is sample image, other is database image
    def Compare(self, other, depth):
        tolerance = 20

        #value images on lowest fails:comparisons ratio
        fails = 0
        comparisons = 0

        coordStrings = ["tl", "tr", "bl", "br"]
        for coordString in startCoords:
            coord = []
            coord.append(coordString)

            selfSample = self.Sample(coord)
            otherSample = other.Sample(coord)


            if Distance(self.Sample()

    def Sample(self, coord):
        if len(coord) == 0:
            return self.average
        else:
            if coord[0] == "tl":
                return tl.Sample(coord[1:])
            elif coord[0] == "tr":
                return tr.Sample(coord[1:])
            elif coord[0] == "bl":
                return bl.Sample(coord[1:])
            elif coord[0] == "br":
                return br.Sample(coord[1:])
            else:
                print "Unrecognised coord:", coord[0]
                return -1

    def __init__(self, image, boxMinInit, boxMaxInit):
        self.average = Vec3(0, 0, 0)
        pixelCount = 0
        for x in range(boxMin.x, boxMax.x):
            for y in range(boxMin.y, boxMax.y):
                Vec3.Add(pixel.r, pixel.g, pixel.b)
                pixelCount += 1
        self.average.Divide(pixelCount)




#load image
#quad = Quadtree(image, Vec2(0,0), Vec2(image.width, image.height))
